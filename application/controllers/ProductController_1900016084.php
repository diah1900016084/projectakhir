<?php 
class Products_controller_1900016097 extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('Products_1900016097');
    }
    public function getProduct(){
        $data['getPoduct'] = $this->Products_1900016097->getProducts();
        $this->load->view('client', $data);
    }
    public function getProductId($id_product){
        $data = $this->db ->get_where('product',['id_product'=> $id_product])->row_array();
        $this->load->view('client', $data);
    }
    public function postProduct(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->Products_1900016097->addProduct($data);
    }
    public function deleteProduct( $id_product = NULL ){
        $data = array('id_product' => $id_product );
        $this->Products_1900016097->deleteProduct($data);
    }
    public function putProduct( $id_barang = NULL ){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $data = array(
            'id_product' => $id_product,
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->Products_1900016097->updateProduct($data);
    }
}
?>