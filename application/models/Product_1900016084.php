defined('BASEPATH') OR exit('No direct script access allowed');
class product extends CI_Model {
    public function get_product(){
        $this->db->select('*');
        $this->db->from('product');
        return $this->db->get()->result();
    }
    public function add($data){
        $this->db->insert('product', $data);
    }
    public function update($data){
        $this->db->where('id_product', $data['id_product']);
        $this->db->update('product', $data);
    }
    public function delete($data){
        $this->db->where('id_product', $data['id_product']);
        $this->db->delete('product', $data);
    }
}
?>